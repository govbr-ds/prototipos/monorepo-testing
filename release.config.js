const sharedConfig = require("@govbr-ds/release-config");

module.exports = {
  branches: [...sharedConfig.branches],
  plugins: [
    sharedConfig.plugins.commitAnalyzer,
    sharedConfig.plugins.releaseNotes,
    sharedConfig.plugins.changelog,
    sharedConfig.plugins.gitlab,
    [
      "@semantic-release/exec",
      {
        prepareCmd: "pnpm exec nx release version ${nextRelease.version}",
        publishCmd: "pnpm exec nx release publish --tag ${nextRelease.channel}",
      },
    ],
    [
      "@semantic-release/git",
      {
        assets: [
          "package.json",
          "CHANGELOG.md",
          "packages/tokens/package.json",
          "packages/utilitarios/package.json",
        ],
        message:
          "chore(release): ${nextRelease.version} [skip ci] \n\n${nextRelease.notes} \n\nCommit gerado automaticamente durante o processo de release",
      },
    ],
  ],
};
