# CHANGELOG

## [2.0.0](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v1.37.0...v2.0.0) (31/01/2024)


### 💥 BREAKING CHANGES

* **api:** The previous /login endpoint is now deprecated and will be removed in the next major release. Use the new /authenticate endpoint for user authentication.

### ✨ NOVIDADES

* **api:** introduce new endpoint for user authentication ([58d3018](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/58d3018c627e5df46ca8533bfef04759a3b6dc7d))
* nx and pnpm new project ([fff23b8](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/fff23b84c779e00077a0811ba6dc7c947749df2e))
* nx and pnpm new project ([cc0d606](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/cc0d606c9a79e47663761b30ceaa8af196bc94d3))
* nx and pnpm new project ([698dad2](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/698dad22851eab779ba6e66e159e8866062e2117))
* nx and pnpm new project ([528bc1d](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/528bc1dc647903f13092deccf6defe7f6faf4609))
* nx and pnpm new project ([df60ac2](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/df60ac2cd1a4290e02ff4fbb832d5decec8d1912))
* nx and pnpm new project ([0cc4f1a](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/0cc4f1abb4347bc304347e8cb142cb366b819b28))
* nx and pnpm new project ([5757470](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/5757470427c3941870b8e9f83927048a03c5f2c4))
* nx and pnpm new project ([6c63d77](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/6c63d77e240157b6dd622c285cec8263d445b6eb))
* nx and pnpm new project ([be18abd](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/be18abd0531d4f734ac34239aefb0e8dfc9b6175))
* nx and pnpm new project ([c8a12e6](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/c8a12e6874429913f043829475e98bd6352fa9b1))
* nx and pnpm new project ([cde261a](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/cde261a59316e905882abaaf0aa9a79dcfd43047))
* nx and pnpm new project ([a4971b1](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/a4971b19075e5c1607d3fa33b26da7a4b2a5bf7f))
* nx and pnpm new project ([37a3b52](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/37a3b52f50dae8479a381a145b2c6cc7de0b74e3))
* nx and pnpm new project ([51770da](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/51770da47a24a45e3dc453b7dd0c7ac5ac4c9526))
* nx and pnpm new project ([19391f5](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/19391f51de02bb2743a85019669a75bdaeba0c7f))
* nx and pnpm new project ([80e17c5](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/80e17c51516aad7324c1df1925606bee634869e6))
* nx and pnpm new project ([0dcc448](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/0dcc448531e38499cfc2be0f5d9cb6ca60e22a9c))
* nx and pnpm new project ([fb12ac5](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/fb12ac592f99d85d9bc8df95d88d5c513806237a))
* nx and pnpm new project ([bd861f6](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/bd861f6ccb765985cf7f7c1197f6a2d77660617b))
* nx and pnpm new project ([401c128](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/401c128e0dc031cd810a89353e6ec6fb50a79dbe))
* nx and pnpm new project ([b507c6f](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/b507c6f0c18d6c1e6935abe51f506b16b37edae9))
* nx and pnpm new project ([50cfc0a](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/50cfc0a1e049c564af16a9655a0e57fd087e24cd))
* nx and pnpm new project ([24bea3b](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/24bea3bb7b7745ba9d6d71b43348ddaf7aee351a))
* nx and pnpm new project ([02a9bad](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/02a9bade8d235c661969fcce36456fafa09ea6e4))
* nx and pnpm new project ([733a1e4](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/733a1e4019d0acc44bb76315ce665e4751b21676))
* nx and pnpm new project ([e34e67f](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/e34e67f9b24f424d66d118a5e6985c454c165047))
* nx and pnpm new project ([31c5604](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/31c560473dce7ef44ebed6fcbd6409a7d4aaf0ba))
* nx and pnpm new project ([ef3136c](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/ef3136caa72d805edbb352c79a197a96fb188b06))
* nx and pnpm new project ([11c05da](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/11c05da2ff4ad5423f2281e4b0f791ceae348ccc))
* nx and pnpm new project ([c6a0477](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/c6a0477b305bd23537af08795aae5358ebd3e20a))
* nx and pnpm new project ([b02df14](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/b02df14198825ab100b9104830e3d29c7af1577e))
* nx and pnpm new project ([fd9bcbc](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/fd9bcbc76eb4280ec3cf303dc15799bd7b817699))
* nx and pnpm new project ([c5311ce](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/c5311ceb3da46209fe04d573855716a091419429))
* nx and pnpm new project ([c3da658](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/c3da658d76e40d1b96fe10fed1e4df1175d52279))
* nx and pnpm new project ([be3fb30](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/be3fb30153775f758dd3f6b5e607f860c1d74200))
* nx and pnpm new project ([8073312](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/8073312f358a7c2adaee777a1f8213adb0a3abc4))
* nx and pnpm new project ([f629ffb](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/f629ffbe53a9df274125735a51cb352513ccf37d))
* nx and pnpm new project ([7facd49](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/7facd493c5928d8149038ab3e4008b46b2c0c1e7))
* nx and pnpm new project ([1bf9679](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/1bf96791b43ff65e7b0b62af8fb4731737703073))
* nx and pnpm new project ([f7b760a](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/f7b760a7558bbe9d4ee1a5b4ff642046a497f2a6))
* nx and pnpm new project ([7c0986a](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/7c0986a578f9d75fbe94a7f15e1f14c7fef50ca0))
* nx and pnpm new project ([4e6493f](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/4e6493f65ec3362078f1b7e3246cf2ba98a1b014))
* nx and pnpm new project ([4410ddf](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/4410ddf715d89c2deab78d518f53fc58cb705ac9))
* nx and pnpm new project ([4fb1d7d](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/4fb1d7dba6d5d94745ce184150ac4ea217455834))
* nx and pnpm new project ([9f99317](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/9f993178f176a7db12deeb184060401a2afef465))
* nx and pnpm new project ([a9732f4](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/a9732f4343b773f7aad53f5af516099aefcff617))
* nx and pnpm new project ([a69797d](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/a69797dc0f60682d5498cd49721378e6cf17dc78))
* nx and pnpm new project ([c80e86f](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/c80e86f61066b3142dbd18eca96e3cd5a98a7dc5))


### 🐛 CORREÇÕES

* remove a dependencia de tokens dos utilitários ([817fed2](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/817fed2df7828cdfee4376d6d10bd750f1fdbac1))

## [2.0.0-next.14](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.13...v2.0.0-next.14) (31/01/2024)


### 🐛 CORREÇÕES

* remove a dependencia de tokens dos utilitários ([817fed2](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/817fed2df7828cdfee4376d6d10bd750f1fdbac1))

## [2.0.0-next.13](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.12...v2.0.0-next.13) (29/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([fff23b8](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/fff23b84c779e00077a0811ba6dc7c947749df2e))

## [2.0.0-next.12](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.11...v2.0.0-next.12) (29/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([cc0d606](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/cc0d606c9a79e47663761b30ceaa8af196bc94d3))
* nx and pnpm new project ([698dad2](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/698dad22851eab779ba6e66e159e8866062e2117))

## [2.0.0-next.11](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.10...v2.0.0-next.11) (29/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([528bc1d](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/528bc1dc647903f13092deccf6defe7f6faf4609))

## [2.0.0-next.10](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.9...v2.0.0-next.10) (29/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([df60ac2](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/df60ac2cd1a4290e02ff4fbb832d5decec8d1912))
* nx and pnpm new project ([0cc4f1a](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/0cc4f1abb4347bc304347e8cb142cb366b819b28))

## [2.0.0-next.9](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.8...v2.0.0-next.9) (29/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([5757470](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/5757470427c3941870b8e9f83927048a03c5f2c4))

## [2.0.0-next.8](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.7...v2.0.0-next.8) (29/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([6c63d77](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/6c63d77e240157b6dd622c285cec8263d445b6eb))

## [2.0.0-next.7](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.6...v2.0.0-next.7) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([be18abd](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/be18abd0531d4f734ac34239aefb0e8dfc9b6175))
* nx and pnpm new project ([c8a12e6](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/c8a12e6874429913f043829475e98bd6352fa9b1))
* nx and pnpm new project ([cde261a](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/cde261a59316e905882abaaf0aa9a79dcfd43047))
* nx and pnpm new project ([a4971b1](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/a4971b19075e5c1607d3fa33b26da7a4b2a5bf7f))
* nx and pnpm new project ([37a3b52](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/37a3b52f50dae8479a381a145b2c6cc7de0b74e3))
* nx and pnpm new project ([51770da](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/51770da47a24a45e3dc453b7dd0c7ac5ac4c9526))

## [2.0.0-next.6](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.5...v2.0.0-next.6) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([19391f5](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/19391f51de02bb2743a85019669a75bdaeba0c7f))

## [2.0.0-next.5](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.4...v2.0.0-next.5) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([80e17c5](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/80e17c51516aad7324c1df1925606bee634869e6))

## [2.0.0-next.4](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.3...v2.0.0-next.4) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([0dcc448](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/0dcc448531e38499cfc2be0f5d9cb6ca60e22a9c))
* nx and pnpm new project ([fb12ac5](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/fb12ac592f99d85d9bc8df95d88d5c513806237a))
* nx and pnpm new project ([bd861f6](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/bd861f6ccb765985cf7f7c1197f6a2d77660617b))
* nx and pnpm new project ([401c128](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/401c128e0dc031cd810a89353e6ec6fb50a79dbe))
* nx and pnpm new project ([b507c6f](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/b507c6f0c18d6c1e6935abe51f506b16b37edae9))

## [2.0.0-next.3](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.2...v2.0.0-next.3) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([50cfc0a](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/50cfc0a1e049c564af16a9655a0e57fd087e24cd))
* nx and pnpm new project ([24bea3b](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/24bea3bb7b7745ba9d6d71b43348ddaf7aee351a))
* nx and pnpm new project ([02a9bad](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/02a9bade8d235c661969fcce36456fafa09ea6e4))
* nx and pnpm new project ([733a1e4](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/733a1e4019d0acc44bb76315ce665e4751b21676))

## [2.0.0-next.3](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.2...v2.0.0-next.3) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([24bea3b](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/24bea3bb7b7745ba9d6d71b43348ddaf7aee351a))
* nx and pnpm new project ([02a9bad](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/02a9bade8d235c661969fcce36456fafa09ea6e4))
* nx and pnpm new project ([733a1e4](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/733a1e4019d0acc44bb76315ce665e4751b21676))

## [2.0.0-next.3](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.2...v2.0.0-next.3) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([02a9bad](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/02a9bade8d235c661969fcce36456fafa09ea6e4))
* nx and pnpm new project ([733a1e4](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/733a1e4019d0acc44bb76315ce665e4751b21676))

## [2.0.0-next.2](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v2.0.0-next.1...v2.0.0-next.2) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([e34e67f](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/e34e67f9b24f424d66d118a5e6985c454c165047))

## [1.37.0-alpha.4](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v1.37.0-alpha.3...v1.37.0-alpha.4) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([4fb1d7d](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/4fb1d7dba6d5d94745ce184150ac4ea217455834))

## [1.37.0-alpha.3](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v1.37.0-alpha.2...v1.37.0-alpha.3) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([9f99317](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/9f993178f176a7db12deeb184060401a2afef465))
* nx and pnpm new project ([a9732f4](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/a9732f4343b773f7aad53f5af516099aefcff617))

## [1.37.0-alpha.2](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v1.37.0-alpha.1...v1.37.0-alpha.2) (25/01/2024)

### ✨ NOVIDADE

* nx and pnpm new project ([a69797d](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/a69797dc0f60682d5498cd49721378e6cf17dc78))

## [1.37.0-alpha.1](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v1.36.0...v1.37.0-alpha.1) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([c80e86f](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/c80e86f61066b3142dbd18eca96e3cd5a98a7dc5))
* nx and pnpm new project ([239ece9](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/239ece958d54634879c8e0ba0bba6b1765f15b5c))
* nx and pnpm new project ([7ede060](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/7ede0601db2d284142bfdc24f64651cc0a38ae95))

## [1.36.0](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v1.35.0...v1.36.0) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([d69b59f](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/d69b59f84f6567e16ce2d39f70f34eec810c416a))
* nx and pnpm new project ([8a58e5f](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/8a58e5fbe84e1fb945d2518cf3967b2f30bf51f3))
* nx and pnpm new project ([5227df6](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/5227df6ea85a2c3c3a8c68fde5907939575c1fb7))

## [1.35.0](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v1.34.0...v1.35.0) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([4243ceb](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/4243ceb31dbaf91aa3d5036d272b9b77474c182b))

## [1.34.0](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v1.33.0...v1.34.0) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([eb0c175](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/eb0c17592035e6b15f72f6e4ca134bc711c5721a))
* nx and pnpm new project ([cd14c5c](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/cd14c5cff4a20eb5cfe427c2a548e738be288103))

## [1.33.0](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v1.32.0...v1.33.0) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([52a3ac7](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/52a3ac73a51f3ce3579cf3085eeb356315f7138e))

## [1.32.0](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v1.31.0...v1.32.0) (25/01/2024)

### ✨ NOVIDADES

* nx and pnpm new project ([022b437](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/022b437c202b748631b66b9c000f4255cda780f4))

## [1.31.0](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/compare/v1.30.0...v1.31.0) (25/01/2024)

### ✨ NOVIDADES

* **create-turbo:** apply official-starter transform ([2b676fe](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/2b676fe9ecbf94739cb2e4530783a691f6a81892))
* **create-turbo:** apply official-starter transform ([aa1ee24](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/aa1ee24a0e501dc05b8e0734d8fd8b88a8fed3bd))
* **create-turbo:** apply pnpm-eslint transform ([8f89165](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/8f89165c8b5547e21171309d1e5fea61d66ab20f))
* **create-turbo:** apply pnpm-eslint transform ([b9fa44d](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/b9fa44d53525a3fe435a761a0dddf2e0bca50978))
* **create-turbo:** install dependencies ([af65cdf](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/af65cdfb3e2885e7847d9bcdba6e07540938f9ef))
* **create-turbo:** install dependencies ([7e604d2](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/7e604d216b0f70c1ddfc2b40d61806c423846904))
* lerna new project ([563e1cc](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/563e1cc43b3c76e25d2eeb65929a32c339b844d5))
* lerna new project ([650f09f](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/650f09f19b48fad9dd94d1170194cd22e356b3cc))
* lerna new project ([ba53e0a](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/ba53e0a8e72d2672913e98b3f22fbe4d8928bf9b))
* lerna new project ([004ad95](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/004ad95a2b67ba5b58eba510ac639006c4d43c9d))
* lerna new project ([54a33c2](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/54a33c25d7b091a1840996169e232ab663cae937))
* nx and pnpm new project ([6a7ad19](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/6a7ad19b1f6d7af3e7355536b1927cd34e9176ea))
* nx and pnpm new project ([fc307fd](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/fc307fd7b22d7b2a4facc5a26b914c6c389da298))
* nx and pnpm new project ([57cc4c1](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/57cc4c1f1efbee12389e06617b6c79a5ccafeb61))
* nx and pnpm new project ([f83a646](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/f83a6465155d6b16a010e5941afa55d1391690c7))
* turbo new project ([005906b](https://gitlab.com/govbr-ds/prototipos/monorepo-testing/commit/005906b2347049969f45faa1788b1e5f87a1f2c8))
